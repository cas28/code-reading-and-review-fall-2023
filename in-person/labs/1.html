<h2>Introduction</h2>
<p>For our first group lab exercise, you will work with one or two other students to discuss your code from the pre-lab assignment 1, with the goal of eventually compromising on a single version of the code. You will also get some hands-on practice with using Git collaboratively.</p>
<p>To be clear, this is not <i>quite</i> a realistic collaborative development process: in a real work environment, you will almost never be working on the <i>exact same task</i> as your teammates. (That would not be an ideal use of everyone's time!) In this lab exercise, your teammates have written code for the same task that you have: implementing the "square" and "percent" buttons in this calculator codebase.</p>
<p>With that said, this lab exercise is designed to give you experience with several <i>parts</i> of real-world collaborative development: reading other people's code, having your own code read by others, and collaborating to produce a single copy of a codebase that incorporates the contributions of multiple developers.</p>
<h2>Setting up your group's repository</h2>
<p>The instructions in this section should be done on a <strong>single</strong> group member's computer and GitLab account, but everyone in the group should pay attention to the process!</p>
<p>First, create a GitLab group for your team: open <a href="https://gitlab.cecs.pdx.edu">https://gitlab.cecs.pdx.edu</a>, click the button with the <code>+</code> icon in the top bar, and select "New group" and then "Create group". You may choose any group name you want (within reason).</p>
<p>Set these options in your new group:</p>
<ul>
    <li>Visibility level: private</li>
    <li>Role: Software developer</li>
    <li>Who will be using this group?: My company or team</li>
    <li>What will you use this group for?: I want to learn the basics of Git</li>
</ul>
<p>Next, open the "Group information" tab and select "Members". Click the "Invite members" button, select the role "Maintainer", enter the emails for each member of your project <strong>and <code>cas28@pdx.edu</code></strong>, and then click "Invite".</p>
<p>Finally, make a <strong>new</strong> fork of the <a href="https://gitlab.cecs.pdx.edu/cas28/calculator">original calculator repository</a>. In the "Project URL" dropdown, <strong>select your group name, not your username</strong>.</p>
<h2>Pushing your code to your group's fork</h2>
<p>The instructions in this section should be done for <strong>each</strong> group member on their own computer, not just on a single computer for the whole group.</p>
<p>Open your Calculator project from the pre-lab assignment. (If you did the pre-lab assignment on a different computer than the laptop that you're using for this lab, clone your pre-lab repository onto your laptop first.)</p>
<p>Make sure all of your pre-lab changes have been committed and pushed: run <code>git status</code> in the terminal to double-check that you have no uncommitted changes in your local copy of the repository.</p>
<p>Add your newly-created group repository as a new remote in your local repository. Use the "clone with SSH" link as the remote repository URL, and use <code>lab1</code> as the name of your new remote. You can do this in VSCode with the GitLens plugin by clicking the "Add remote" button in the "Remotes" pane, or in the terminal with the <code>git remote add</code> command.</p>
<p>Now you need to push your code to a <strong>new branch on the new remote</strong>. Open the terminal and run this command:</p>
<blockquote><code>git push lab1 main:<strong>your_username_here</strong></code></blockquote>
<p>For example, <strong>my</strong> username is <code>cas28</code>, so <strong>I</strong> would run the command <code>git push lab1 main:cas28</code>. Your username is different than mine, so your command will look a little different.</p>
<p>This command takes the branch named <code>main</code> in your local repository and pushes it to your group's fork in a new branch, with the name of the new branch set to your username.</p>
<p>After you've run this command to push your new branch, you should be able to see your branch in your group's fork, with your username as the branch name.</p>
<h2>Fetching your other group members' code</h2>
<p>Once every member of your group has pushed their own branch to the group's fork, you will each need to clone from the group's fork so that <strong>each</strong> group member has a copy of <strong>every</strong> branch from your group on their own laptop.</p>
<p>You can do this the same way that you cloned the code in pre-lab assignment 1 - just <strong>make sure that you're cloning from your group's fork, not your personal fork</strong>. This will get you a new copy of the repository which contains each of your group members' branches.</p>
<p>At this point, you're finished with the clone that you made in the pre-lab assignment. For the rest of this lab, you will work in this new clone of your group's fork.</p>
<h2>Reviewing each group members' code</h2>
<p>Now that each group member can access each branch on their own machine, it's time to discuss your code!</p>
<p>Open the "Source Control" sidebar in VSCode and expand the "Remotes" pane. Right-click the "origin" remote and select "Fetch". This will ensure that you have access to all of the branches in the repository.</p>
<p>To compare two branches, in the "Remotes" pane, right click one branch and choose "Select for compare", then right click the other branch and choose "Compare with selected". This will populate the "Search &amp; Compare" pane with information about what is different between each branch.</p>
<p>To compare two different versions of the same file across different branches, after you've done "Select for compare" and "Compare to selected", right click the file in the "Search &amp; Compare" pane and select "Open changes".</p>
<p>Within your group, discuss the differences between your solutions. What differences in formatting, ordering, and naming choices can you find? Each member of the group should explain to the group why they wrote the code the way that they did. Try to idenfity parts of your group members' code that may potentially be improvements on your own code.</p>
<h2>Merging into the main branch</h2>
<p>After discussing the differences in your code, your next goal is to produce a <strong>single</strong> version of the code that reflects what you think is the "best" version of these changes that your group can collectively produce.</p>
<p>To be clear, you still <strong>should not change any of the provided code</strong> in the calculator project! You are not trying to come up with the best overall version of this <strong>codebase</strong>, just the best version of your group's <strong>changes</strong>.</p>
<p>Together, decide on an order to merge each of your individual branches into the main branch of your group's codebase. Be warned, this will be a messy process: starting with the second merge, you will most likely end up with merge conflicts that you need to resolve!</p>
<p>This part will be done on a single computer, but the whole group should be involved in making the decisions about how to fix each merge conflict. For each of your individual branches in sequence:</p>
<ul>
    <li>Merge the branch into the <code>main</code> branch of the repository.</li>
    <li>Resolve any merge conflicts that come up, making sure to leave the modified code in a state that the group agrees is ideal. You may do some "clean-up" work during the merge conflict resolution process.</li>
</ul>
<p>After merging the last branch, your <code>main</code> branch should now have a copy of your group's collective changes, in a version that the group has compromised on. Make sure to run this final version of the code and check that it behaves the way that you expect!</p>
<p>It is okay if the final copy of the code in your <code>main</code> branch only includes code from some of the group members' individual branches, but each group member must be involved in the process of discussing and choosing which changes to keep, and in the process of merging the changes together.</p>
<h2>Finishing up</h2>
<p>If you've followed the instructions this far, you're almost done with the lab!</p>
<p><strong>Before you leave the lab session</strong>, run one last <code>git push</code> in the <code>main</code> branch of your repository and double-check that all of your group's work is visible in the <code>main</code> branch of your group's fork on GitLab.</p>
<p>Once your final <code>main</code> code is pushed, you're done for today and you can leave the lab session whenever you want!</p>
<p><strong>Within a week of the lab session</strong>, submit your post-lab writeup to the Canvas Assignments page. The post-lab writeup is <strong>individual work</strong> that each student must submit. Review the syllabus for general details.</p>
<p>For this lab, your post-lab writeup should specifically cover:</p>
<ul>
    <li>a reflection on how your group members' pre-lab assignment code was different from your own,</li>
    <li>a description of your group's discussions <strong>in your own words</strong>,</li>
    <li>and a description of how your group resolved any merge conflicts that came up during the merging part of the lab, with a focus on how you chose which code to keep, which to discard, and how to do the "clean-up" step.</li>
</ul>
