<html>
<head>
<style>
  body { max-width: 8in; margin: 1in; }
  table { border: 1px solid black; border-collapse: collapse; }
  th, td { border: 1px solid black; padding: 0.3em; }
  th { text-align: left; }
  td { text-align: center; }
</style>
</head>
<body>

<h1>CS 410/510 Code Reading &amp; Review, Fall 2023</h1>
<h2>Syllabus: Online section</h2>

<h3>Academic misconduct</h3>
  <p>All assignments are <strong>individual work</strong> unless the assignment explicitly says that group work is allowed.</p>
  <p>When you submit individual work, you are implicitly claiming that <strong>you alone</strong> are the author of the content that you are submitting, except any content that you have explicitly cited a source for. If this claim is not true, your submission is plagiarism.</p>
  <p>If you <strong>implicitly or explicitly</strong> claim authorship of anything that you did not personally write, you will get a grade of zero on your submission. This also applies if you knowingly allow someone else to claim authorship of your own work. If it happens more than once, I will write you up for academic misconduct.</p>
  <p>Review PSU's rules at <a href="https://www.pdx.edu/dos/academic-misconduct">https://www.pdx.edu/dos/academic-misconduct</a>.</p>

<h3>Repository</h3>
  <div>The syllabus and schedule for this course will be tracked in a GitLab repository at <a href="https://gitlab.cecs.pdx.edu/cas28/code-reading-and-review-fall-2023">https://gitlab.cecs.pdx.edu/cas28/code-reading-and-review-fall-2023</a>. This will let you see the history of any changes that I may make to these documents throughout the quarter.</div>

<h3>Course staff</h3>
  <p>Please <a href="mailto:cas28@pdx.edu,vpoulo@pdx.edu">email <b>both of us</b></a> if you have a question!</p>
  <table>
    <tr><td>
      <div>Katie Casamento</div>
      <div>Email: cas28@pdx.edu</div>
      <div>Office hours: Thursday 4-5pm, in the fishbowl or my office (FAB 115D), or at <a href="https://pdx.zoom.us/j/81175448449">https://pdx.zoom.us/j/81175448449</a></div>

      <p>FAB 115D is in the CS offices behind the fishbowl, near the bottom-right of page 9 in the <a href="https://www.pdx.edu/buildings/sites/g/files/znldhr2301/files/2021-10/Fourth%20Avenue%20Building%20Floorplans.pdf">building plan</a> (where L120-00 is the fishbowl).</p>
    </td></tr>
    <tr><td>
      <div>Vaughn Poulo</div>
      <div>Email: vpoulo@pdx.edu</div>
      <div>Office hours: Tuesday 1-2pm, at <a href="https://pdx.zoom.us/j/87417214057">https://pdx.zoom.us/j/87417214057</a></div>
    </td></tr>
  </table>


<h3>Sections</h3>
  <p>There are two <b>different</b> sections of this course in Fall 2023. This syllabus is for the <b>Online</b> section, <b>not the In-Person section</b>. Both sections will cover the same overall material, but the presentation and requirements will be different between sections.</p>

  <p>Compared to the In-Person section, the Online version gives you more freedom to manage your own schedule. You will have the option to collaborate with other students on a project, but you will not be required to work in a group at any point. You will always have the option to watch the recording of a lecture instead of attending synchronously.</p>

  <p>In exchange, you will have more quizzes and assignments to complete than the students in the In-Person section, who have required in-person labs to complete instead. You will have plenty of time to complete all of the work before the quarter is over, but only if you apply good time management skills. Nobody will stop you from falling behind the lecture schedule, but if you fall too far behind it will be hard to catch up before the quarter ends.</p>

<h3>Lecture</h3>
  <p>Lecture is Monday and Wednesday 2:00-3:50PM, at <a href="https://pdx.zoom.us/j/82840806627">https://pdx.zoom.us/j/82840806627</a>.</p>
  <div>Lecture recordings will be available on Canvas within 48 hours of each live lecture.</div>
  <p>Please <b>keep your microphone muted when you're not talking</b> so it doesn't pick up noise in your environment.</p>

<h3>Reading</h3>
  <div>There will be required readings posted to Canvas throughout the quarter.</div>

<h3>Course discussion forum</h3>
  <p>We'll be using the <a href="https://fishbowl.zulip.cs.pdx.edu/">Zulip server hosted by the CAT at PSU</a> for course discussion. You should have already received an invite to the <a href="https://fishbowl.zulip.cs.pdx.edu/#narrow/stream/258-crr-online-fall-2023">course discussion stream</a>. If you haven't received an invite, please email me!

<h3>Assignments</h3>
  <p>There will be four assignments, which will attempt to simulate parts of a (somewhat) realistic collaborative software development process, working in a (somewhat) realistic development environment. Some assignment work will have you interacting with other students in the class, but each assignment submission must be your own individual work. The assignment details will be posted to the Canvas and discussed in lecture.</p>
  <p>The assignments will primarily use the <a href="https://www.typescriptlang.org/">TypeScript</a> language. In the first two weeks of lecture, we'll spend some time introducing TypeScript and getting everyone set up with a development environment. I recommend using an IDE for these assignments, especially if you've never worked with an IDE before; I'll walk through setup instructions for Visual Studio Code in lecture, but if you already have a favorite IDE, you can probably easily install a TypeScript plugin for it.</p>
  <p>I strongly recommend <strong>not</strong> doing all of your work remotely on the CS department Linux servers for this course. Instead, try to follow along in the first couple weeks of lecture when we talk about setting up a development environment, and set up your environment directly on your computer. If you don't have a suitable computer to work on, you may be able to <a href="https://library.pdx.edu/study-spaces-computers/equipment/">borrow a laptop from the PSU library</a>.</p>

<h3>Quizzes</h3>
  <p>Each week, there will be a "code review quiz" on Canvas designed to give you practice in the topics that we've discussed in lecture. These will usually involve both writing in English and editing some provided code in TypeScript.</p>
  <p>Most quiz responses will be graded on how well you justify your answers, not on whether you get the "right answers". There will often be certain answers that I expect, but it's fine if your answer is different than the one I expect as long as you can justify it.</p>

<h3>Course project</h3>
  <p>Graduate students are required to complete a course project. You may work alone or in groups of two or three, but no more than three.</p>

  <p>For the course project, you will select a <b>large codebase that at least one member of your group has previously worked on</b>. Your goal is to <b>significantly improve the quality and maintainability of the codebase</b>, according to the principles that we cover in class.</p>

  <p>At the end of the course, each graduate student will submit two project deliverables:
    <ul>
      <li>A link to a Git repository with your group's work, hosted on a site like GitLab or GitHub.</li>
      <li>A writeup explaining your own individual contributions to the project.</li>
    </ul>
  </p>


<h3>Deadlines</h3>
  <p>See the course schedule document on Canvas for the release dates and due dates of the assigned coursework.</p>
  <p>The deadlines for the quizzes and course project are each "hard due dates": you must submit by these dates in order to get credit for your work.</p>
  <p>The assignment deadlines are "soft due dates". If you submit an assignment by its soft due date, you will get a grade and feedback on your submission within one week of the soft due date.</p>
  <p>If you submit an assignment after its soft due date, I guarantee that your submission will be graded for full credit by the end of the quarter, but I'll grade it <b>whenever it's convenient for me</b>, which might be very late in the quarter. That's the deal!</p>

<h3>Resubmissions</h3>
  <p>As an incentive to submit assignments and quizzes "on time", you have a limited opportunity to resubmit for an updated grade after your submission has been graded. <b>If you submit an assignment by its soft due date</b>, then after you get back your grade, you may resubmit once for an updated grade.</p>
  <p>Assignment 5 is an exception: there won't be time for resubmissions since it's due at the end of the quarter.</p>
  <p>A resubmission has a hard due date of two weeks after the original soft due date. This leaves you with at least one week to rework your submission after receiving a grade and feedback on it.</p>
  <p>Resubmissions after this hard due date will not be graded, and if you don't submit the an assignment by its soft due date, then resubmissions will not be graded at all for that assignment.</p>

<h3>Grading</h3>
  <p>Grading will be very subjective for this course, since most of the material is subjective. For each piece of work you submit, you will receive feedback and a <b>letter grade</b>. Grades will be recorded on the Canvas page for this course. Final grades will be curved up at my discretion.</p>
  <p>In your final grade, each assignment is "worth" approximately as much as two quizzes. For graduate students, the final project grade is like an additional assignment grade.</p>
  <p>I expect to give most students final grades in the A to B range, so please don't stress too much about your grade - focus on the course content!</p>

</body>
</html>
