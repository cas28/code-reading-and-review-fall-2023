<h2>Introduction</h2>

<p>This assignment builds on your quiz 4 work to explore how <i>encapsulation</i> is used to achieve maintainable code structures. This will be the subject of our week 5 lectures, so please be sure to watch and review those as part of your work process on this assignment!</p>

<p>For this project, imagine that you are now assigned to work with a team of <b>human translators</b> who will localize the English text of this game into other natural languages. Your goal is to structure the code so that new translations can be added with minimal code changes.</p>

<p>There are already translations into Indonesian and Japanese, which you can find here: <a href="https://docs.google.com/spreadsheets/d/1w0J4vwGRBFHC2_0oY-JPvwsYinARa2ScNR4Rl_0HbEg/edit?usp=sharing">https://docs.google.com/spreadsheets/d/1w0J4vwGRBFHC2_0oY-JPvwsYinARa2ScNR4Rl_0HbEg/edit?usp=sharing</a>. You should update the quiz 4 code to work in both of these languages, and to easily support adding more translations later.</p>

<p>Please understand: <b>your job is to work WITH these human translators. DO NOT ATTEMPT TO AUTOMATE THEM OUT OF A JOB.</b> Every quarter I get a couple submissions that think this is a good use case for the Google Translate API. This is a <b>very bad idea</b> in the real world: <b>localization must be done by human professionals</b>. The global field of software has learned this the hard way through decades of bad localizations. If your users want to use Google Translate, <b>they can use Google Translate just fine without your help</b>. Your users do not benefit from you using Google Translate for them. The Google Translate API is for building apps that enable users to use Google Translate <b>for themselves</b>; the Google Translate API is <b>not for automatically localizing the text of an application</b>.</p>


<h2>Setup</h2>

<p>You should start with a copy of your code from quiz 4. You may refactor your quiz 4 code in any way that you want. By the end of your work in this quiz, your code should behave the same as your code in quiz 4, but with the additional feature of extensible translations.</p>


<h2>Code requirements</h2>

<p>At the very start of the game, before anything else is printed, your code should print a message like this using <code>prompt</code>:</p>
<blockquote>
  Enter EN to play in English.<br>
  Masukkan ID untuk bermain dalam Bahasa Indonesia.<br>
  「JA」を入れて日本語で遊ぶ。
</blockquote>

<p>This message should include all of the languages that your code supports: if your code is extended with new translations later, this message should get updated. The two-letter language identifiers like EN, ID, and JA are <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639-1</a> codes. The user will enter one of these identifiers (in uppercase) to select their language. If the user enters an invalid identifier, the program should print the string "Invalid input." in <i>every</i> language that your code supports, using a single call to <code>console.err</code> (not a separate call for each language).</p>

<p>After this point, the program should do all of its input and output in the language that the user has selected. If the program is restarted by calling <code>play()</code> again, it should ask for the user's language again.</p>

<p>You <b>may</b> use any TypeScript features and any programming techniques in your code, but you should not <b>need</b> anything other than what we've covered in lecture.</p>

<p>You may modify the <code>src/Main.ts</code> file and add additional <code>.ts</code> files inside the <code>src</code> folder, but you <b>must not modify any of the project settings</b> outside the <code>src</code> folder (<code>package.json</code>, <code>tsconfig.json</code>, etc.).</p>


<h2>Hints</h2>

Here are some hints to help get you started on adding the features described above.

<h3>Data abstraction</h3>

<p>You should be able to find a way to structure the code so that a translator can add a new translation just by <b>adding new code</b> and <b>modifying a single existing value</b>. The process of adding a new translation should <b>not</b> require <b>modifications to multiple different parts</b> of the code.</p>

<p>Plan your refactoring before you start typing! A little data abstraction will go a long way.</p>

<h3>Imports</h3>

<p>If you want to structure your code across multiple files, you must use the <code>export</code> keyword to declare any types, variables, or functions that you want to access from a different file. Anything declared without <code>export</code> is "private" to the file it's declared in.</p>

<p>To import something named <code>foo</code> from a file named <code>Bar.ts</code> in the same folder as the current file, write <code>import { foo } from "./Bar"</code>. See the assignment 1 code for some examples.</p>

<h3>OOP or not?</h3>

<p>For this exercise, the game itself is considered "finished": there will not be any new gameplay features added to the game over time.</p>

<p>The set of translations <b>is</b> expected to change over time, as translators add new translations to the existing gameplay.</p>

<p>Consider carefully whether this seems like "adding new behaviors to existing data" or "adding new data to existing behaviors". This should inform your choice of how to structure the code, as we'll discuss in the week 5 lectures.</p>


<h2>Submitting your code</h2>

<p>When you're finished, zip up your <code>src</code> folder and upload it to this page.</p>
