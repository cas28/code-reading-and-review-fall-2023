<h2>Introduction</h2>
<p>For our first exercise, you're going to add some features to a little graphical calculator application.</p>
<p>This isn't <i>directly</i> related to code review, but it's likely that many students in this course, especially undergraduates, have never been part of a modern collaborative software development process. I think that before we can really begin talking about reading and reviewing code, it's important to work through an illustration of that kind of process, so that everyone has a baseline for what it looks like and what the tools we use are capable of.</p>
<p>In some ways, <strong>this assignment is also a test of your ability to read instructions carefully and follow them correctly</strong>. You should re-read this document many times before you start writing any code.</p>
<p>If you're struggling, it will help to review the lecture videos! We will talk through this assignment in the week 2 lectures.</p>
<p>Now: imagine you just started a job, and your first task before you start any real work is to learn how to build, read, and modify the codebase with the tools that your team uses.</p>
<h2>Getting the code</h2>
<p>Download the code from <a href="https://gitlab.cecs.pdx.edu/cas28/calculator/-/archive/main/calculator-main.zip">https://gitlab.cecs.pdx.edu/cas28/calculator/-/archive/main/calculator-main.zip</a>.</p>
<p><strong>Make sure to save your project folder with a file path that does not include the <code>&amp;</code> character.</strong> For example, don't save your project as <code>C:\Users\Katie\Documents\Code Reading <strong>&amp;</strong> Review\Calculator</code>. One of the tools in our build system can't handle paths like this.</p>
<p>Like in the TypeScript intro project, you will need to run the <code>npm i</code> command <strong>once</strong> to set up the project.</p>
<h2>Building the code</h2>
<p>For each npm console command mentioned in the README, there is an equivalent command in the VSCode "Run Build Task" menu.</p>
<p>Build the project and open the <code>html/index.html</code> file in a web browser to see the calculator application. Try it out! Notice the behavior if you press the = key multiple times after entering an operation, and how it updates the screen with intermediate results during complex operations with multiple operators.</p>
<h2>Linting the code</h2>
<p>This codebase has a code style guide which is enforced by ESLint. <strong>The code that you submit must not have any ESLint warnings.</strong> You must also not use any <code>eslint-disable</code> comments to disable ESLint warnings.</p>
<p>To run ESLint from the terminal, you can run <code>npm run lint</code>. This will identify all formatting errors in your code. If you have no formatting errors, <code>npm run lint</code> will just exit without printing any warnings.</p>
<p>To run ESLint from VSCode, you can install the <a href="https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint">ESLint plugin</a>. This will show ESLint warnings directly in the text of a file as you're editing it.</p>
<p>Regardless of how you choose to run ESLint, <strong>make sure to check that ESLint is working</strong> before you submit your code. You can do this by deliberately formatting something incorrectly and then checking that ESLint catches it properly.</p>
<h2>Modifying the code</h2>
<p>The middle two buttons in the top row on the calculator don't do anything yet. Your job is to implement their behavior.</p>
<p>The button with the text "^3" on it is a "cube" operation: when pressed, it should replace the number on screen with the result of cubing that number (raising it to the power of 3).</p>
<p>The button with the % symbol on it is a "percent" symbol (not "modulo"): when pressed, it should replace the number on screen with the result of dividing that number by 100.</p>
<p>For either button, if the screen is showing "Infinity", "-Infinity", or "NaN" before the button is pressed, it should continue showing one of these values after the button is pressed. (It is okay if pressing a button changes "Infinity" to "NaN", for example, but it shouldn't change "Infinity" to "0".)</p>
<p>These are the only requirements for the behavior of your code. For anything left unspecified in these requirements, you can make your own decisions about how the buttons should behave. Make sure to document any of your decisions that might not be obvious!</p>
<p>You will need to update <code>src/Calculator.ts</code>, <code>src/CalculatorUI.ts</code>, and <code>src/Main.ts</code> in order to implement the behavior for these buttons. You do not need to add unit tests in <code>src/Calculator.test.ts</code>, but it can be helpful for checking the correctness of your own code.</p>
<p>Do not modify any existing code in this project except for adding your new code.</p>
<h2>Submitting your work</h2>
<p>Submit <strong>only</strong> the three TypeScript files that you modified. Do not rename the files. (Don't worry if Canvas renames them automatically after you submit them, just make sure they have the correct names before you submit them.)</p>
<p>For all work in this course, you may resubmit as many times as you want before the deadline. (See the syllabus for the policy on submissions after the deadline.) If you resubmit, <strong>make sure to resubmit all of the files</strong>, even if there are some files that haven't changed since your last submission. This makes it easier to grade your resubmission.<strong></strong></p>
<h2>Grading</h2>
<p>Your code must compile with no warnings or errors. This includes errors from ESLint: you <strong>will</strong> lose points for formatting or linting errors. We will cover how to use ESLint correctly in lecture.</p>
<p>If your code compiles correctly, works correctly, and is submitted correctly, you will get an A. This assignment is just meant to get you a little bit of hands-on experience with what it looks like to extend someone's code in a modern development environment.</p>
